import React from 'react';
import { connect } from 'react-redux';
import 'antd/dist/antd.css';

import PublicView from 'Views/PublicView/PublicView';
import PrivateView from 'Views/PrivateView/PrivateView';
import Spinner from 'Components/Spinner/Spinner';

import { auth } from 'Components/App/selectors';
import { appAuthListener } from 'Components/App/actions';

import { GlobalStyles } from './styles/GlobalStyles';

class App extends React.Component {
  componentDidMount() {
    this.props.appAuthListener();
  }
  render() {
    return (
      <div className="App">
          <GlobalStyles/>
          { this.props.auth.isAuthorized ?
          <PrivateView /> : <PublicView /> }
          <Spinner visible={this.props.auth.fetching} />
      </div>
    );
  }
}

export default connect( state => ({
  auth: auth(state),
}), dispatch => ({
  appAuthListener: params => dispatch(appAuthListener(params))
}) )(App);
