import { Layout as AntLayout } from 'antd';
import styled from 'styled-components';

const Layout = styled(AntLayout)`
    &.ant-layout {
        height: 100vh;
    }
`;

export {
    Layout
}