import React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './styles';

import Sider from 'Components/Chat/Sider/Sider';
import ConversationContainer from 'Components/Chat/ConversationContainer/ConversationContainer';
import UserInfoModal from 'Components/User/InfoModal/InfoModal';
import ChatCreateModal from 'Components/Chat/CreateModal/CreateModal';
import ChatSettingsModal from 'Components/Chat/SettingsModal/SettingsModal';

const ChatLayout = () => {
    return (
        <Layout>
            <Sider/>
            <Route path="/c/:id" component={ConversationContainer} />
            <UserInfoModal />
            <ChatCreateModal />
            <ChatSettingsModal />
        </Layout>
    )
}

export default ChatLayout
