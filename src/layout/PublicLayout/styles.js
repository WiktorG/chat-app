import { Layout as AntLayout } from 'antd';
import styled from 'styled-components';

const Layout = styled(AntLayout)`
    &.ant-layout {
        height: 100vh;
    }
`;

const Content = styled(AntLayout.Content)`
   display: flex;
   justify-content: center;
   align-content: center;
   align-items: center;
   flex-direction: column;
   .app__title {
       margin-bottom: 0;
   }
`;

const Footer = styled(AntLayout.Footer)`
    text-align: center;
`;

const Bold = styled.a`
    font-weight: 600;
`

export {
    Layout, Content, Footer, Bold
}