import React from 'react'

import { Layout, Content, Footer, Bold } from './styles';

import LoginForm from './../../components/LoginForm/LoginForm';
import RegisterForm from './../../components/RegisterForm/RegisterForm';

const PublicLayout = () => {
    return (
        <Layout>
            <Content>
                <h2 className="app__title">
                    Chatzakka
                </h2>
                <LoginForm />
                <RegisterForm />
            </Content>
            <Footer>
                Created with Techno by <Bold href="https://wiktor.me">Wiktor Gała</Bold> &copy;
            </Footer>
        </Layout>
    )
}

export default PublicLayout
