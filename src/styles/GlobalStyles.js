import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
    #root {
        min-width: 1000px;
    }

    .ant-modal {
        max-width: 420px;
        width: calc( 100% - 10px ) !important;
    }
    
    .ant-checkbox-wrapper {
        line-height: 1.1em;
    }

    .ant-upload.ant-upload-select.ant-upload-select-picture-card {
        border-radius: 50%;
    }

    .upload-preview {
        max-width: 102px;
        max-height: 84px;
        border-radius: 50%;
        object-fit: cover;
        width: 100%;
        height: 100%;
    }
`

export { GlobalStyles }