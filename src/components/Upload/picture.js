import { message } from 'antd';

const photoUpload = base64 => {

}

const beforeUpload = (file) => {
    const isJPG = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJPG) {
      message.error('You can only upload JPG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 10;
    if (!isLt2M) {
      message.error('Image must smaller than 10MB!');
    }
    return isJPG && isLt2M;
  }

export {
    photoUpload,
    beforeUpload,
}