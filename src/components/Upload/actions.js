import fire from 'Services/firebase'

const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

// You can send null to get promise in return
const uploadFile = (file, dir) => {
    if (file !== null) {
        const timestamp = new Date().getTime();
        const filePath = dir+'/'+timestamp+'_'+file.name
        const storageRef = fire.storage().ref(filePath);
        return storageRef.put(file).then(res => {
            if (res.state === 'success') {
                return { 
                    status: 'uploaded', 
                    path: filePath,
                    url: fire.storage().ref(filePath).getDownloadURL(),
                };
            }
        });
    } else { 
        return Promise.resolve({status: 'file_not_set', path: '', url: Promise.resolve('')});
    }
}

export {
    getBase64,
    uploadFile
}