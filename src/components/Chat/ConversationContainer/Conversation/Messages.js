import React, { Component } from 'react'
import { connect } from 'react-redux'

import { chatCurrent } from 'Components/Chat/selectors';
import { markMessageAsRead } from 'Components/Chat/actions'; 
import { user } from 'Components/User/selectors';
import { Avatar, Spin } from 'antd';



class Messages extends Component {

    scrollDown = () => {
        const messagesContainer = document.querySelector('.js-conversation');
        messagesContainer.scrollTop = document.querySelector('.chat__messages').offsetHeight;
    }


    UNSAFE_componentWillReceiveProps(nextProps) {
        if ( !nextProps.chatCurrent.messages.pending || !this.props.chatCurrent.members.pending ) {
            window.setTimeout( this.scrollDown, 1 );
        }
    }

    render() {
        const urlify = (text) => {
            const urlRegex = /(https?:\/\/[^\s]+)/g;
            const matches = urlRegex.test(text);
            if (matches && text.split(' ').length !== 1) {
                const newText = text.split(' ').map((item, i) => {
                    if ( urlRegex.test(item) ) {
                        return <a href={item} target="_blank" rel="noopener noreferrer">{item}</a>;
                    }
                    return item
                });
                return newText;
            } else if (matches && text.split(' ').length === 1) {
                return <a href={text} target="_blank" rel="noopener noreferrer">{text}</a>;
            }
            return text
        }

        const messages = (() => { 
        if (
            this.props.chatCurrent.messages.pending || 
            this.props.chatCurrent.members.pending || 
            this.props.chatCurrent.messages.notFound 
        ) {
            return (
                <div
                    style={{
                        width: '100%',
                        height: '100vh',
                        position: 'absolute',
                        display: 'flex',
                        justifyContent: 'center',
                        alignContent: 'center',
                        alignItems: 'center',
                        paddingBottom: '70px',
                    }}
                >
                    {this.props.chatCurrent.messages.notFound && !this.props.chatCurrent.messages.pending ? 
                        <h3>I didn't find any messages for this chat</h3>
                    : 
                        <Spin 
                            size={'default'}
                            tip={`Wait a second. I'm getting your messages`}
                        />
                    }
                </div>
            )
        }
        return this.props.chatCurrent.messages.list.map(message => {
            let fromFetched = {};
            this.props.chatCurrent.members.fetched.forEach(member => {
                if ( message.from === member.id ) { 
                    fromFetched = member 
                }
                if (fromFetched.id === this.props.user.id) {
                    fromFetched = {
                        id: this.props.user.id,
                        ...this.props.user.data,
                    }
                }
            });

            if ( message.seenBy.indexOf(this.props.user.id) === -1 ) {
                this.props.markMessageAsRead(this.props.chatCurrent.id, message.id, [...message.seenBy, this.props.user.id]);
            }

            const messageToShow = message.message.split("\\n").map((item, i) => {
                return <p key={`${message.id}with${i}`}>{urlify(item)}</p>;
            })

            return (
                <li
                    className={`chat__message ${message.from === this.props.user.id ? 'chat__message--own' : '' }`}
                    key={message.id}
                >
                    <Avatar 
                        size="default"
                        src={fromFetched.imgUrl}
                        icon="user"
                        className="avatar"
                    >
                    </Avatar>
                    <span className="author">
                        {fromFetched.nickname !== undefined ? fromFetched.nickname : 'chatzakka user'}
                    </span>
                    <span className="content">
                        {messageToShow}
                    </span>
                </li>
            );

            });
        })();

        return (
            <ul className="chat__messages">
                {messages}
            </ul>
        )
    }
}

export default connect(state => ({
    chatCurrent: chatCurrent(state),
    user: user(state),
}), dispatch => ({
    markMessageAsRead: (chatId, messageId, seenBy) => dispatch(markMessageAsRead(chatId, messageId, seenBy)),
}) )(Messages)
