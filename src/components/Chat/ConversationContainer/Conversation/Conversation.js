import React, { Component } from 'react'
import { connect } from 'react-redux'

import { chatCurrent } from 'Components/Chat/selectors'

import { ChatConversationStyled } from './styles';

import Messages from './Messages';
import Form from './Form';

class ChatConversation extends Component {
    render() {
        return (
            <ChatConversationStyled className="js-conversation">
                <Messages />
                <Form />
            </ChatConversationStyled>
        )
    }
}

export default connect(state => ({
    chatCurrent: chatCurrent(state),
}), dispatch => ({

}))(ChatConversation)
