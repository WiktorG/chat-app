import React, { useState } from 'react'
import { connect } from 'react-redux'

import { user } from 'Components/User/selectors';
import { chatCurrent } from 'Components/Chat/selectors';

import { addMessageToChat } from 'Components/Chat/actions';

import { Input, Icon, Button } from 'antd'
const { TextArea } = Input;

const Form = (props) => {
    const [message, setMessage] = useState('');

    const scrollDown = () => {
        const messagesContainer = document.querySelector('.js-conversation');
        messagesContainer.scrollTop = document.querySelector('.chat__messages').offsetHeight;
    }

    const resizeChatContainer = () => window.setTimeout(() => {
            const conversationContainer = document.querySelector('.js-conversation');
            const messageForm = document.querySelector('.js-message__form');
            conversationContainer.setAttribute("style", `height: calc( 100vh - ${ messageForm.offsetHeight }px )`);
            window.setTimeout( scrollDown, 200 );
    }, 200);

    const sendMessage = () => {
        const messageToSend = {
            message: message.replace(/\n/g, '\\n'),
            images: [],
            from: props.user.id,
            seenBy: [
                props.user.id,
            ],
            sentAt: Date.now(),
        };
        if (message !== '') {
            props.addMessageToChat(props.chatCurrent.id, messageToSend);
            setMessage('');
            const conversationContainer = document.querySelector('.js-conversation');
            conversationContainer.removeAttribute("style");
        }
    }

    const handleEnterPress = e => !e.shiftKey && message.replace( /(?:\r\n|\r|\n)/g, '' ) !== '' ? 
        (() => { 
            e.preventDefault();
            sendMessage();
        })() 
        : 
        message === '' ? e.preventDefault() : resizeChatContainer()
         
    const handleBackspacePress = e => {
        const keyCode = e.which || e.keyCode || e.charCode;
        if ( keyCode === 8 || keyCode === 46 ) {
           resizeChatContainer(); 
        }
    }

    return (
        <div 
        className={`message__form js-message__form`}
        >
            <TextArea
                className="message__textarea"
                placeholder="Type your message..."
                autosize={{ minRows: 2, maxRows: 6 }}
                onPressEnter={ e => handleEnterPress(e) }
                onKeyDown={ e => handleBackspacePress(e) }
                onChange={e => setMessage(e.target.value)}
                value={message}
                form="false"
            />
            <div className="message__controls">
                <div className="message__icons">
                    <Icon type="delete" onClick={ () => { setMessage(''); resizeChatContainer(); } } />
                </div>
                <Button 
                    size="default" 
                    type="primary"
                    shape="round"
                    onClick={sendMessage}
                >
                    Send
                </Button>
            </div>
        </div>
    )
}

export default connect(state => ({
    user: user(state),
    chatCurrent: chatCurrent(state),
}), dispatch => ({ 
    addMessageToChat: (chatId, message) => dispatch(addMessageToChat(chatId, message))
}))(Form)
