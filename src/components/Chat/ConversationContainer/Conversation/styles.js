import styled from 'styled-components';

const ChatConversationStyled = styled.div`
    & {
        position: relative;
        width: 100%;
        height: calc( 100vh - 70px );
        overflow-y: scroll;
        transition: all 0.3s ease;
        &::-webkit-scrollbar {
            display: none;
        }
    }

    & .chat__messages {
        list-style: none;
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        align-items: flex-start;
        align-content: center;
        width: 100%;
        height: auto;
        margin: 0;
        padding: 5px 5px 5px 5px;
    }

    & .chat__message {
        position: relative;
        display: flex;
        word-break: keep-all;
        margin: 16px 0 2px;
        width: 100%;
    }

    & .chat__message--own {
        margin-left: auto;
        flex-direction: row-reverse;
    }

    & .chat__message .avatar { 
        margin-right: 2.5px;
        margin-left: 2.5px;
        user-select: none;
    }


    & .chat__message .author {
        font-size: 11px;
        line-height: 10px;
        position: absolute;
        width: auto;
        top: -2px;
        transform: translateY(-100%);
        left: 50px;
        user-select: none;
    }

    & .chat__message--own .author {
        left: unset;
        right: 50px;
    }

    & .chat__message .content {
        display: block;
        background-color: #40A9FB;
        color: #FFFFFF;
        min-width: 40px;
        font-weight: 500;
        padding: 7px 12px;
        border-radius: 18px;
        width: auto;
        max-width: 60%;
        word-break: break-word;
        box-shadow: 0px 0px 15px rgba(64, 169, 251, 0.1);
        p {
            margin: 0;
            min-height: 21px;
        }
        a {
            display: block;
            color: inherit;
            text-decoration: underline;
        }
    }

    & .chat__message--own .content {
        background-color: #FFFFFF;
        color: #40A9FB;
    }

    & .message__form {
        position: fixed;
        bottom: 0;
        min-height: 70px;
        width: calc( 100% - 600px );
        min-width: 400px;
        padding: 0 4px;
        background-color: #FFFFFF;
        border-top: solid 1px #f5f5f5;
        display: flex;
        flex-direction: row;
        align-content: center;
        justify-content: center;
        align-items: flex-end;
        transition: all 0.3s ease;
    }

    & .message__form--expanded {
        height: 170px;
    }

    & .message__form .message__textarea {
        width: 95%;
        resize: none;
        height: 60px;
        border-radius: 15px;
        padding-left: 15px;
        transition: all 0.3s ease;
        margin-bottom: 5px;
        margin-top: 5px;
        padding-top: 8px;
        padding-bottom: 8px;
        font-size: 14px;
    }

    .message--width {
        display: block;
        height: 0;
        opacity: 0;
        position: absolute;
    }

    & .message__form--expanded .message__textarea { 
        height: 160px;
    }

    & .message__controls { 
        display: flex;
        flex-direction: row;
        justify-content: flex-end;
        align-items: center;
        align-content: center;
        margin-bottom: 4px;
    }

    & .message__icons {
        display: flex;
        flex-direction: row;
        margin-left: 5px;
    }

    & .message__icons i {
        font-size: 17px;
        margin-right: 5px;
        transition: all 0.2s ease;
        cursor: pointer;
        &:hover { 
            color: #40A9FB;
            transform: rotate(10deg);
        }
    }

`

export { 
    ChatConversationStyled
}