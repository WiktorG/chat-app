import { Layout as AntLayout } from 'antd';
import styled from 'styled-components';

const Content = styled(AntLayout.Content)`
    & {        
        width: 100%;
        height: 100vh;
        display: flex;
        flex-direction: row;
    }
`

export {
    Content
}