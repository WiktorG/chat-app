import React, { Component } from 'react'
import { connect } from 'react-redux'

import { chatCurrent } from 'Components/Chat/selectors';
import { user } from 'Components/User/selectors';

import { Avatar, Skeleton } from 'antd';


class ChatMembers extends Component {
    render() {
        let chatMembers = this.props.chatCurrent.members.fetched.filter(member => member.id !== this.props.user.id).map(member => {
            return (
                <li
                    key={member.id}
                    className="member"
                >
                    <Avatar 
                        src={member.imgUrl}
                        icon="user"
                    >
                        {member.nickname}
                    </Avatar>
                    <span className="member__nickname">
                        {member.nickname}
                    </span>
                </li>
            )
        })
        chatMembers.push((
            <li
                    key={this.props.user.id}
                    className="member"
                >
                    <Avatar 
                        src={this.props.user.data.imgUrl}
                        icon="user"
                    >
                        {this.props.user.data.nickname}
                    </Avatar>
                    <span className="member__nickname">
                        {this.props.user.data.nickname}
                    </span>
            </li>
        ));
        return (    
            <div className="chat-info__block">
                <h2 className="chat-info__block-title">Members</h2>
                {this.props.chatCurrent.members.pending ? 
                <Skeleton 
                    paragraph={false}
                />
                : 
                <ul className="chat-info__members">
                    {chatMembers}
                </ul>
                }
                
            </div>
        )
    }
}

export default connect(state => ({
    chatCurrent: chatCurrent(state),
    user: user(state),
}), dispatch => ({
}) )(ChatMembers)
