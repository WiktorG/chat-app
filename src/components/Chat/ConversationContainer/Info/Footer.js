import React from 'react'

import { Bold } from 'Layouts/PublicLayout/styles';

export default () => {
  return (
    <div 
        className="chat-info__block chat-info__footer"
    >
      <span>Created with Techno by <Bold href="https://wiktor.me">Wiktor Gała</Bold> &copy;</span>
    </div>
  )
}
