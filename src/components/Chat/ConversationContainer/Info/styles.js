import styled from 'styled-components';

const ChatInfo = styled.div`
    & {
        position: relative;
        display: flex;
        flex-direction: column;
        background-color: #FFFFFF;
        height: 100vh;
        width: 100%;
        max-width: 300px;
    }

    & .chat-info__edit-icon {
        position: absolute;
        top: 20px;
        right: 20px;
        font-size: 19px;
        transition: all 0.2s ease;
        cursor: pointer;
        z-index: 1;
        &:hover { 
            color: #40A9FB;
            transform: rotate(-10deg);
        }
    }

    & .chat-info__header-skeleton {
        padding-bottom: 5px;
        padding-top: 5px;
    }

    & .chat-info__header-skeleton .ant-skeleton-paragraph li {
        width: 100% !important;
    }

    & .chat-info__header { 
        position: relative;
        width: 100%;
        padding: 10px 20px;
        border-bottom: solid 1px #f5f5f5;
    }

    & .chat-info__avatar {
        height: 70px;
        width: 70px;
        left: 50%;
        transform: translateX(-50%);
        display: flex;
        justify-content: center;
        align-content: center;
        align-items: center;
    }

    & .chat-info__avatar img {
        object-fit: cover;
        width: 100%;
        height: 100%;
    }


    & .chat-info__header .chat-info__header-title {
        margin-bottom: 0;
        margin-top: 5px;
        text-align: center;
    }

    & .chat-info__header .ant-skeleton {
        transform: translateY(8px);
    }

    & .chat-info__block {
        padding: 7px 20px 11px;
        border-bottom: solid 1px #f5f5f5;
    }

    & .chat-info__block-title {
        text-align: center;
        margin-top: 0px;
        margin-bottom: 7px;
    }

    & .chat-info__members {
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-content: flex-start;
        align-items: flex-start;
        flex-wrap: wrap;
        list-style: none;
        margin-top: 0;
        margin-bottom: 0;
        padding: 0;
    }

    & .chat-info__members li {
        margin-top: 2.5px;
        margin-bottom: 2.5px;
        padding-right: 8px;
        border-radius: 25px;
        margin-right: 2px;
        margin-left: 2px;
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-content: center;
        align-items: center;
        background-color: #FFFFFF;
        color: #41A9FB;
        cursor: pointer;
        transition: all 0.2s ease;
        .ant-avatar {
            box-shadow: 0px 0px 15px rgba(64, 169, 251, 0.1);
            min-width: 32px;
        }
    }
    
    & .chat-info__members h3 {
        margin-bottom: 0;
    }

    & .chat-info__members .member__nickname {
        margin-bottom: 0;
        margin-left: 5px;
        font-weight: 700;
        color: inherit;
    }
`

export {
    ChatInfo
}