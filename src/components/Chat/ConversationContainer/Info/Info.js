import React from 'react'
import { connect } from 'react-redux'

import { chatCurrent } from 'Components/Chat/selectors';
import { toggleModal } from 'Components/Modals/actions';
import ChatEditModal from 'Components/Chat/EditModal/EditModal';

import Header from './Header';
import ChatMembers from './Members';


import Footer from './Footer';

import { ChatInfo as ChatInfoStyled } from './styles';

import { Icon } from 'antd';

const ChatInfo = (props) => {
    return (
        <ChatInfoStyled>
            {!props.chatCurrent.pending && !props.chatCurrent.members.pending ? 
            <>
            <Icon
                    className="chat-info__edit-icon"
                    type="edit"
                    onClick={() => props.toggleModal('chatEdit')}
            />
            <ChatEditModal />
            </>
            : 
            void null
            }
            <Header />
            <ChatMembers />
            <Footer />
        </ChatInfoStyled>
    )
}

export default connect( state => ({
    chatCurrent: chatCurrent(state),
}), dispatch => ({
    toggleModal: (modalName) => dispatch(toggleModal(modalName)),
}) )(ChatInfo)
