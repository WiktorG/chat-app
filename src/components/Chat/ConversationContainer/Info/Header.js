import React, { Component } from 'react'
import { connect } from 'react-redux';

import { chat } from 'Components/Chat/selectors';

import { Skeleton, Avatar } from 'antd'

class Header extends Component {

    shouldComponentUpdate(nextProps) {
        return ( (this.props.chat.current.imgUrl !== nextProps.chat.current.imgUrl) || (this.props.chat.current.name !== nextProps.chat.current.name) )
    }

    render() {
        return (
            <div className="chat-info__header">
                <Avatar 
                        src={this.props.chat.current.imgUrl} 
                        size='large'
                        className="chat-info__avatar"    
                    >
                    {this.props.chat.current.pending ? '' : this.props.chat.current.name.substr(0, 1)}
                </Avatar>
                {this.props.chat.current.pending ? 
                    <Skeleton 
                        active 
                        title={false}
                        paragraph={{rows: 1}} 
                        className="chat-info__header-skeleton"
                        />
                :
                    <h1 className="chat-info__header-title">
                        {this.props.chat.current.name}
                    </h1>
                }
            </div>
        )
    }
}

export default connect(state => ({
    chat: chat(state),
}), dispatch => ({
}) )(Header)
