import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Helmet } from 'react-helmet'

import store from '../../../services/store';

import { chatReducer, getCurrentChat } from '../actions';

import { chat } from '../selectors';
import { user } from '../../User/selectors';

import { Content } from './styles';

import ChatConversation from 'Components/Chat/ConversationContainer/Conversation/Conversation';
import ChatInfo from 'Components/Chat/ConversationContainer/Info/Info';

class ConversationContainer extends Component {

    componentDidMount() {
        this.props.getCurrentChat(this.props.match.params.id, this.props.user.id);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if ( this.props.match.params.id !== nextProps.match.params.id ) {
            this.props.getCurrentChat(nextProps.match.params.id, this.props.user.id);
            // if chat changes - unsubscribe (don't listen for changes) to previous chat
            !this.props.chat.current.pending && !this.props.chat.current.notFound ? 
            this.props.chat.current.unsubs.forEach(unsub => unsub()) : void null;
        }
        
    }

    render() {
        return (
            <Content>
                {this.props.chat.current.notFound ? 
                <>
                    <Helmet>
                        <title>ChatZakka - Woops!</title>
                    </Helmet>
                    <h1>Woops!</h1>
                    <h2>You are trying to do something that you are not allowed to!</h2>
                </>
                : 
                <>
                    <Helmet>
                        <title>ChatZakka - {`${this.props.chat.current.name}`}</title>
                    </Helmet>
                    <ChatConversation />
                    <ChatInfo />
                </>
                }
            </Content>
        )
    }
}

store.attachReducers({ chat: chatReducer })

export default connect(state => ({
    chat: chat(state),
    user: user(state),
}), dispatch => ({
    getCurrentChat: (chatId, userId) => dispatch(getCurrentChat(chatId, userId)),
}) )(ConversationContainer)