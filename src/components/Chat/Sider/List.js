import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import moment from 'moment'

import { getChatList } from './../actions'

import { chatCurrent, chatList } from 'Components/Chat/selectors'
import { user } from 'Components/User/selectors'

import { Avatar, Badge } from 'antd'
import { List as StyledList } from './styles'

class List extends Component {
  
  componentDidMount() {
      this.props.getChatList(this.props.user.id);
  }

  render() {
    const items = this.props.chatList.items.map(item => {
        return (
            <li 
                key={item.id}
                className={`
                    ${this.props.chatCurrent.id === item.id ? 'sider__chat-link--active' : ''} 
                    ${item.lastMessage !== null && item.lastMessage.seenBy.indexOf(this.props.user.id) === -1 ? ' sider__chat-link--new-msg' : ''} 
                `}
            >
                <Link 
                    to={`/c/${item.id}`}
                    className={`sider__chat-link`}    
                >
                    <Badge
                        dot={item.lastMessage !== null && item.lastMessage.seenBy.indexOf(this.props.user.id) === -1 ? true : false}
                        className="chat-link__badge"
                    >
                        <Avatar
                            size='large'
                            src={item.imgUrl}
                        >
                        {item.name.substr(0, 1)}
                        </Avatar>
                    </Badge>

                    <span className="chat-link__info-container">
                        <span className="link__info-title">
                            {item.name}
                        </span>
                        { item.lastMessage !== null ? 
                            <span className="link__last-message-container">
                                <span className="last-message__content">
                                    {item.lastMessage.message.length < 12 ? item.lastMessage.message : `${item.lastMessage.message.substr(0, 12)}..`}
                                </span>
                                <span className="last-message__date">
                                    {moment().subtract(new moment().diff(new moment(item.lastMessage.sentAt))).calendar()}
                                </span>
                            </span>    
                        : void null }
                    </span>
                </Link>
               
            </li>
        )
    })
    return (
      <StyledList>
       {items}
      </StyledList>
    )
  }

}

export default connect(state => ({
    chatList: chatList(state),
    chatCurrent: chatCurrent(state),
    user: user(state),
}), dispatch => ({
    getChatList: params => dispatch(getChatList(params)),
}) )(List)
