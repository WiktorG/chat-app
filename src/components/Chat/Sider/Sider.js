import React from 'react'
import { Sider as AntSider } from './styles';

import Header from './Header';
import Controls from './Controls';
import List from './List';

const Sider = () => {
    return (
        <AntSider
            theme="light"
            width={300}
        >
            <Header />
            <List />
            <Controls />
        </AntSider>
    )
}

export default Sider
