import React from 'react'
import { connect } from 'react-redux'

import { toggleModal } from 'Components/Modals/actions'

import { Icon } from 'antd'
import { Controls as StyledControls } from './styles';

const Controls = (props) => {
    return (
        <StyledControls>
            <Icon type="user" onClick={() => props.toggleModal('userInfo')} />
            <Icon type="plus" onClick={() => props.toggleModal('chatCreate')}/>
            <Icon type="setting" onClick={() => props.toggleModal('settings')}/>
        </StyledControls>
    )
}

export default connect(state => ({

}), dispatch => ({
    toggleModal: modalName => dispatch(toggleModal(modalName)),
}) )(Controls)
