import { Layout as AntLayout } from 'antd';
import styled from 'styled-components';

const Sider = styled(AntLayout.Sider)`
    &.ant-layout-sider {
        width: 300px;
        position: relative;
        padding: 70px 0;
        overflow-y: scroll;
    }
`;

const Header = styled.div`
    & {
        position: absolute;
        top: 0;
        left: 0;
        display: flex;
        justify-content: flex-start;
        align-content: center;
        align-items: center;
        width: 100%;
        height: 70px;
        padding: 10px 20px;
        border-bottom: solid 1px #f5f5f5;
    }

    & > div {
        transform: translateY(8px);
        width: 100%;
    }

    h3 {
        margin-left: 16px;
        margin-bottom: 0;
    }

    img {
        object-fit: cover;
        width: 100%;
        height: 100%;
    }
`;

const Controls = styled.div`
    & {
        position: absolute;
        bottom: 0;
        left: 0;
        display: flex;
        justify-content: space-between;
        align-content: center;
        align-items: center;
        width: 100%;
        height: 70px;
        padding: 10px 30px;
        border-top: solid 1px #f5f5f5;
    }

    & .anticon {
        font-size: 25px;
        transition: all 0.2s ease;
        cursor: pointer;
        &:hover {
            color: #3BA4FF;
        }
    }
`

const List = styled.ul`
    & {
        display: flex;
        flex-direction: column
        list-style: none;
        width: 100%;
        justify-content: flex-start;
        align-content: flex-start;
        align-items: flex-start;
        margin: 0;
        padding: 0;
    }

    & > li {
        position: relative;
        min-height: 70px
        width: 100%;
        display: flex;
        justify-content: flex-start;
        align-content: center;
        align-items: center;
        padding: 0 10px;
    }

    & .sider__chat-link {
        display: flex;
        justify-content: flex-start;
        align-content: center;
        align-items: center;
        position: relative;
        width: 100%;
        min-height: 70px;
        color: inherit;
    }

    & .sider__chat-link--active {
        background-color: rgba(0, 0, 0, 0.05);
    }

    & .ant-avatar img {
        object-fit: cover;
        width: 100%;
        height: 100%;
    }

    & .chat-link__info-container {
        margin-left: 5px;
        display: flex;
        flex-direction: column;
        align-items: flex-start
        justify-content: center:
        align-content: center; 
        width: calc(100% - 50px);
    }
    
    & .link__info-title {
        font-weight: 500;
        color: #000000;
    }

    & .link__last-message-container {
        display: flex;
        justify-content: space-between;
        align-content: center;
        align-items: center;
        width: 100%;
    }

    & .last-message__date {
        font-size: 11px;
    }

    & .last-message__content {
        font-size: 13px;
    }

    & .chat-link__badge .ant-badge-dot {
        width: 12px;
        height: 12px;
        margin-right: 4px;
        margin-top: 4px;
    }
`;

export {
    Sider,
    Header,
    Controls,
    List
}