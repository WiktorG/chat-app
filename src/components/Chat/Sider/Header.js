import React from 'react';
import { connect } from 'react-redux';
import { user } from 'Components/User/selectors';

import { Skeleton, Avatar } from 'antd';
import { Header as StyledHeader } from './styles';

const Header = (props) => {
    return (
        <StyledHeader>
            {props.user.data.fetching ?
            <div> 
                <Skeleton 
                    avatar 
                    active 
                    paragraph={{rows: 0}} 
                    />
            </div>
            : 
            <>
                <Avatar src={props.user.data.imgUrl} size='large'>
                    {props.user.data.nickname ? props.user.data.nickname[0] : void null}
                </Avatar>
                <h3>
                    {props.user.data.nickname}
                </h3>
            </>
            }
        </StyledHeader>
    )
}

export default connect(state => ({
    user: user(state)
}), dispatch => ({

}) )(Header)
