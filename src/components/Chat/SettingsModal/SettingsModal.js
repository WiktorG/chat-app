import React from 'react';
import { connect } from 'react-redux';
import fire from 'Services/firebase';

import { toggleModal } from 'Components/Modals/actions';
import { modals } from 'Components/Modals/selectors';

import { message, Button } from 'antd';
import { Modal } from 'Components/Chat/CreateModal/styles';

const ChatSettingsModal = (props) => {
    const logout = (e) => {
        e.preventDefault();
        fire.auth().signOut()
        .then(function() {
            props.toggleModal('settings');
        })
        .catch(function(error) {
            message.error('Woops - Something went wrong!')
        });
     
    }
    return (
        <Modal
            visible={props.modals.settings}
            footer={false}
            onCancel={() => props.toggleModal('settings')}
            title="ChatZakka settings."
            centered
        >
            <Button type="primary" onClick={logout} style={{width: '100%'}}>Logout</Button>
        </Modal>
    )
}

export default connect(state => ({
    modals: modals(state),
}), dispatch => ({
    toggleModal: modalName => dispatch(toggleModal(modalName)),
}) )( ChatSettingsModal )
