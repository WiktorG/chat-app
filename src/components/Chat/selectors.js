export const chat = state => state.chat;
export const chatList = state => state.chat.list;
export const chatCurrent = state => state.chat.current;
