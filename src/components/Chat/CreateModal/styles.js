import styled from 'styled-components'
import { Modal as AntModal, Upload as AntUpload } from 'antd'

const Modal = styled(AntModal)`
    &.ant-modal {
       max-width: 340px;
    }
`

const Upload = styled(AntUpload)`
    & .ant-upload {
        margin-right: auto;
        margin-left: auto;
        float: none;
    }
`

const UserElement = styled.span`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-content: center;
    align-items: center;
    min-height: 35px;
    border-radius: 17.5px;
    font-weight: 800;
    border: solid 2px #F9626B;
    transition: all 0.2s ease;
    color: #FFFFFF;
    background-color: #F9626B;
    cursor: pointer;
    margin-right: 5px;
    margin-bottom: 5px;
    &:hover {
        border: solid 2px #F9626B;
        background-color: #FFFFFF;
        color: #F9626B;
    }
    & > .nickname {
        margin-left: 5px;
        margin-right: 8px;
    }
`

const Spinner = styled.div` 
    position: absolute;
    left: 0;
    top: 0;
    display: flex;
    justify-content: center;
    align-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.8);
    z-index: 10;
`

export {
    Modal,
    Spinner,
    UserElement,
    Upload
}