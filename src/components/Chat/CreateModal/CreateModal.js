import React, { useState } from 'react';
import { connect } from 'react-redux';

import { chat } from 'Components/Chat/selectors';
import { user } from 'Components/User/selectors';

import { modals } from 'Components/Modals/selectors';

import { getBase64 } from 'Components/Upload/actions'
import { beforeUpload } from 'Components/Upload/picture'
import { toggleModal } from 'Components/Modals/actions';
import { searchUserByNickname } from 'Components/Search/actions';
import { createNewChat } from 'Components/Chat/actions';

import { Form, Input, Icon, Avatar, Col, Row, AutoComplete, Button, Spin } from 'antd';
import { Modal, Spinner, UserElement, Upload } from './styles';

const { Option } = AutoComplete;

const ChatCreateModal = (props) => {
    const [loadingImg, toggleLoading] = useState(false);
    const [chatImg, setChatImg] = useState('');
    const [chatImgFile, setChatImgFile] = useState(null);
    const [foundUsers, setFoundUsers] = useState([]);
    const [selectedUsers, setSelectedUsers] = useState([])

    const handleSubmit = e => {
        e.preventDefault();
        props.form.validateFields((err, values) => {
            if ( selectedUsers.length === 0 ) {
                props.form.setFields({
                    chatUserSearch: {
                        value: values.chatUserSearch,
                        errors: [new Error('Please select at least one friend!')]
                    }
                })
                err = true;
            } else {
                props.form.setFields({
                    chatUserSearch: {
                        value: values.chatUserSearch,
                        errors: null
                    }
                })
            }
            if (!err) {
                const preparedData = {
                    name: values.chatName,
                    creator: props.user.id,
                    members: [...selectedUsers.map(user => user.id), props.user.id],
                    img: chatImgFile,
                    lastMessage: null,
                }
                props.createNewChat(preparedData);
                props.form.resetFields();
                setChatImg('');
                setChatImgFile(null);
                setSelectedUsers([]);
            }
        });
    }
    
    const handleChange = info => {
        info.file.status === 'uploading' ? toggleLoading(true) : void null;
        info.file.status === 'done' || info.file.status === 'error' ? getBase64(info.file.originFileObj, imgUrl => {
            setChatImg(imgUrl);
            setChatImgFile(info.file.originFileObj);
            toggleLoading(false);
        }) : void null;
    }
    
    //Get nicknames of user that value user inserted
    const handleSearch = async (nickname) => {
        const response = await searchUserByNickname(nickname);
        if (response !== undefined) {
            setFoundUsers(response);
        }
    }

    //Select user - check if user exists in selected users array and if he doesn't add him to the state
    const selectUser = ( newUser ) => {
        let found = false;
        selectedUsers.forEach( ( selectedUser ) => {
            if ( selectedUser.id === newUser.id ) {
                found = true;
            }
        });
        if ( !found ) {
            setSelectedUsers([
                ...selectedUsers,
                newUser
            ]);
        }
    }

    //Deselect user - filter array and remove user from state
    const deselectUser = ( userToRemove ) => {
        const newUsers = selectedUsers.filter((user) => {
            return user.id !== userToRemove.id;
        });
        setSelectedUsers(newUsers);
    }

    const ChatImgUploadButton = () => (
        <div>
            <Icon type={ loadingImg ? "loading" : 'plus' } />
            <div className="ant-upload-text">upload chat image</div>
        </div>
    );


    // Options for autocomplete - check if user is selected or creator and if he isnt add him to options
    const options = foundUsers.map(user => { 
        let found = false;

        if ( user.id === props.user.id ) {
            found = true;
        }

        selectedUsers.forEach( ( selectedUser ) => {
            if ( selectedUser.id === user.id ) {
                found = true;
            }
        });
        
        if ( !found ) {
            return (
                <Option
                    key={user.id}
                    value={user.nickname}
                    onClick={() => selectUser(user)}
                >   
                    <Avatar src={user.imgUrl} >
                        {user.nickname}
                    </Avatar>
                    <span style={ {marginLeft: '10px', fontWeight: 800} }>
                        {user.nickname}
                    </span>
                </Option>
            ) 
        } 
        return undefined;
    } ).filter( (option) => {
        return typeof option !== 'undefined';
    } );

    // Map selected users to show them in DOM
    const selectedUsersDOM = selectedUsers.map(user => (
        <UserElement key={user.id} onClick={() => deselectUser(user)}>
            <Avatar src={user.imgUrl} >
                {user.nickname}
            </Avatar>
            <span className="nickname">
                {user.nickname}
            </span>
        </UserElement>
    ) )

    const { getFieldDecorator } = props.form;
    return (
        <Modal
            visible={props.modals.chatCreate}
            footer={false}
            onCancel={() => props.toggleModal('chatCreate')}
            title="Create new chat."
            centered
        >
            <Form
                onSubmit={handleSubmit}
            >
                <Row gutter={8} >
                    <Col 
                        span={24}
                    >
                        <Upload 
                            listType="picture-card"
                            className="avatar-uploader"
                            showUploadList={false}
                            onChange={handleChange}
                            beforeUpload={beforeUpload}
                        >
                            {
                            chatImg !== '' ? 
                            <img src={chatImg} alt="avatar" className="upload-preview" />
                            :
                            <ChatImgUploadButton/>
                            }
                        </Upload>
                    </Col>
                    <Col span={24} style={{marginTop: '10px'}}>
                        <Form.Item>
                            {getFieldDecorator('chatName', {
                                rules: [
                                    { required: true, message: 'Please name your chat!' },
                                    { min: 3, message: 'Chat name must be at lest 3 characters long!'}
                                ],
                            })(
                                <Input
                                    placeholder="Name"
                                />
                            )}
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                        <Form.Item>
                            {getFieldDecorator('chatUserSearch', {

                            })(
                                <AutoComplete
                                    onChange={(nickname) => {handleSearch(nickname)}}
                                    onFocus={() => handleSearch('')}
                                    dataSource={options}
                                    optionLabelProp="value"
                                >
                                    <Input.Search 
                                        placeholder="Find your friends!"
                                    />
                                </AutoComplete>
                            )}
                        </Form.Item>
                    </Col>
                    {selectedUsersDOM.length !== 0 && 
                        <Col span={24} style={{
                            marginBottom: '20px',
                        }}>
                            <div style={{
                                display: 'flex',
                                flexDirection: 'row',
                                flexWrap: 'wrap',
                                justifyContent: 'center',
                                width: '100%',
                            }}>
                            {selectedUsersDOM}
                            </div>
                        </Col>
                    }
                    <Col span={24}>
                        <Button 
                            style={{
                                width: '100%',
                            }}
                            type="primary" 
                            htmlType="submit"
                        >
                            Create
                        </Button>
                    </Col>
                </Row>
            </Form>
            <Spinner style={{display: props.chat.create.pending ? 'flex' : 'none'}}>
                <Spin />
            </Spinner>
        </Modal>
    )
}

export default connect(state => ({
    chat: chat(state),
    modals: modals(state),
    user: user(state),
}), dispatch => ({
    toggleModal: modalName => dispatch(toggleModal(modalName)),
    createNewChat: params => dispatch(createNewChat(params))
}) )( Form.create()(ChatCreateModal) )
