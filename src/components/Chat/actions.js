import fire from './../../services/firebase';
import { message } from 'antd';

import { uploadFile } from './../Upload/actions';
import { toggleModal } from './../Modals/actions';

import PlayNotification from '../Sounds/Notification';

export const CREATE_CHAT_PENDING = 'CREATE_CHAT_PENDING',
             CREATE_CHAT_SUCCESS = 'CREATE_CHAT_SUCCESS',
             CREATE_CHAT_ERROR = 'CREATE_CHAT_ERROR',
             UPDATE_CHAT_PENDING = 'UPDATE_CHAT_PENDING',
             UPDATE_CHAT_SUCCESS = 'UPDATE_CHAT_SUCCESS',
             UPDATE_CHAT_ERROR = 'UPDATE_CHAT_ERROR',
             GET_CHAT_LIST_SUCCESS = 'GET_CHAT_LIST_SUCCESS',
             GET_CHAT_LIST_ERROR = 'GET_CHAT_LIST_ERROR',
             GET_CURRENT_CHAT_PENDING = 'GET_CURRENT_CHAT_PENDING',
             GET_CURRENT_CHAT_SUCCESS = 'GET_CURRENT_CHAT_SUCCESS',
             GET_CURRENT_CHAT_NOT_FOUND = 'GET_CURRENT_CHAT_NOT_FOUND',
             SET_CURRENT_CHAT_UNSUB = 'SET_CURRENT_CHAT_UNSUB',
             GET_CURRENT_CHAT_ERROR = 'GET_CURRENT_CHAT_ERROR',
             GET_CURRENT_CHAT_MEMBERS_PENDING = 'GET_CURRENT_CHAT_MEMBERS_PENDING',
             GET_CURRENT_CHAT_MEMBERS_SUCCESS = 'GET_CURRENT_CHAT_MEMBERS_SUCCESS',
             GET_CURRENT_CHAT_MEMBERS_NOT_FOUND = 'GET_CURRENT_CHAT_MEMBERS_NOT_FOUND',
             GET_CURRENT_CHAT_MEMBERS_ERROR = 'GET_CURRENT_CHAT_MEMBERS_ERROR',
             GET_CURRENT_CHAT_MESSAGES_PENDING = 'GET_CURRENT_CHAT_MESSAGES_PENDING',
             GET_CURRENT_CHAT_MESSAGES_SUCCESS = 'GET_CURRENT_CHAT_MESSAGES_SUCCESS',
             GET_CURRENT_CHAT_MESSAGES_NOT_FOUND = 'GET_CURRENT_CHAT_MESSAGES_NOT_FOUND',
             ADD_MESSAGE_TO_CHAT_PENDING = 'ADD_MESSAGE_TO_CHAT_PENDING',
             ADD_MESSAGE_TO_CHAT_SUCCESS = 'ADD_MESSAGE_TO_CHAT_SUCCESS',
             ADD_MESSAGE_TO_CHAT_ERROR = 'ADD_MESSAGE_TO_CHAT_ERROR',
             MARK_MESSAGE_AS_READ_PENDING = 'MARK_MESSAGE_AS_READ_PENDING',
             MARK_MESSAGE_AS_READ_SUCCESS = 'ADD_MESSAGE_AS_READ_SUCCESS',
             TOGGLE_CURRENT_CHAT_EDIT = 'TOGGLE_CURRENT_CHAT_EDIT';


const initialState = {
    current: {
        pending: true,
        notFound: false,
        id: null,
        members: {
            pure: [],
            fetched: [],
            pending: true,
        },
        messages: {
            pending: true,
            notFound: true,
            list: [],
        },
        unsubs: [],
        edit: {
            pending: false,
            visible: false,
        }
    },
    create: {
        pending: false,
    },
    list: {
        pending: true,
        items: []
    }
}

const createNewChat = params => async dispatch => {
    dispatch({type: CREATE_CHAT_PENDING});
    const imgData = await uploadFile(params.img, 'chat_avatars');
    let dataToPut = {
        name: params.name,
        imgPath: imgData.path,
        imgUrl: await imgData.url,
        members: params.members,
        creator: params.creator,
        lastMessage: params.lastMessage,
        modifiedAt: Date.now(),
    }
    fire.firestore().collection('chats').doc().set(dataToPut).then(res => {
        message.success('Success!');
        dispatch({type: CREATE_CHAT_SUCCESS});
        dispatch(toggleModal('chatCreate'));
    }).catch(err => {
        message.error('Something went wrong!');
        dispatch({type: CREATE_CHAT_ERROR});
    });
}

const updateChat = params => async dispatch => {
    dispatch({type: UPDATE_CHAT_PENDING});
    const imgData = params.img !== null ? await uploadFile(params.img, 'chat_avatars') : null;
    let dataToPut = {
        name: params.name,
        ...(imgData !== null && {
            imgPath: imgData.path,
            imgUrl: await imgData.url,
        }),
        members: params.members,
        creator: params.creator,
        modifiedAt: Date.now(),
    }
    fire.firestore().collection('chats').doc(params.id).set(dataToPut, {
        merge: true,
    }).then(res => {
        message.success('Success!');
        dispatch({type: UPDATE_CHAT_SUCCESS});
        dispatch(toggleModal('chatEdit'));
    }).catch(err => {
        message.error('Something went wrong!');
        dispatch({type: UPDATE_CHAT_ERROR});
    });
}

const getChatList = userId => dispatch => {
    fire.firestore().collection('chats').where('members', 'array-contains', userId).orderBy('modifiedAt', 'desc').onSnapshot( snapshot => {
        snapshot.docChanges().forEach( (change) => { 
            if ( change.type === "modified" ) {
                const modifiedChat = change.doc.data();
                if ( modifiedChat.lastMessage.seenBy.indexOf(userId) === -1 ) {
                   PlayNotification();
                }
            }
        } ) 
        const chats = snapshot.docs.map(doc => doc.exists ? {...doc.data(), id: doc.id } : null );
        dispatch({type: GET_CHAT_LIST_SUCCESS, chats: chats});
    });
}

const getCurrentChatMembers = chatId => async dispatch => {
    dispatch({type: GET_CURRENT_CHAT_MEMBERS_PENDING});
    const unsub = fire.firestore().collection('chats').doc(chatId).onSnapshot( async chatRef => {
        const members = chatRef.data().members;
        const allUsers = await fire.firestore().collection('users').get();
        const chatMembers = allUsers.docs.map(user => members.indexOf(user.id) !== -1 ? ({ ...user.data(), id: user.id }) : null ).filter(member => member !== null);
        dispatch({type: GET_CURRENT_CHAT_MEMBERS_SUCCESS, fetchedMembers: chatMembers});
    } );    
    dispatch({type: SET_CURRENT_CHAT_UNSUB, unsub: unsub})    
}

const getCurrentChatMessages = chatId => async dispatch => {
    dispatch({type: GET_CURRENT_CHAT_MESSAGES_PENDING})
    const unsub = fire.firestore().collection('chats').doc(chatId).collection('messages').orderBy('sentAt', 'asc').onSnapshot(messagesSnapshot => {
        if ( messagesSnapshot.empty ) {
            dispatch({type: GET_CURRENT_CHAT_MESSAGES_NOT_FOUND});
        } else {
            const messages = messagesSnapshot.docs.map( message => ({ ...message.data(), id: message.id }) );
            dispatch({type: GET_CURRENT_CHAT_MESSAGES_SUCCESS, messages: messages})
        }
    });
    dispatch({type: SET_CURRENT_CHAT_UNSUB, unsub: unsub})
}

//TODO: Try to refactor that monster.
const getCurrentChat = (chatId, userId) => async dispatch => {
    dispatch({type: GET_CURRENT_CHAT_PENDING, chatId: chatId});
    const chatsRef = fire.firestore().collection('chats').where('members', 'array-contains', userId);
    chatsRef.get().then(allChats => {
        let foundChat = allChats.docs.map(chat => {
            if (chat.exists && chat.id === chatId) {
                return { ...chat.data(), id: chat.id };
            } else {
                return void null;
            }
        }).filter(el => el !== undefined)[0];
        foundChat !== undefined ? (() => {
            //Here call all functions that are going to retrieve data connected with current chat            
            dispatch(getCurrentChatMessages( chatId ));
            dispatch(getCurrentChatMembers( chatId ))
        })()
        : dispatch({type: GET_CURRENT_CHAT_NOT_FOUND}); 
    });
    const unsub = chatsRef.onSnapshot(allChats => {
        let foundChat = allChats.docs.map(chat => {
            if (chat.exists && chat.id === chatId) {
                return { ...chat.data(), id: chat.id };
            } else {
                return void null;
            }
        }).filter(el => el !== undefined)[0];
        foundChat !== undefined ? (() => {
            if ( foundChat.lastMessage !== null && foundChat.lastMessage.seenBy.indexOf(userId) === -1 ) {
                //Mark lastMessage field as read in chat doc
                fire.firestore().collection('chats').doc(chatId).update({
                    lastMessage: {
                        ...foundChat.lastMessage,
                        seenBy: [...foundChat.lastMessage.seenBy, userId],
                    },
                })
            }
            dispatch({type: GET_CURRENT_CHAT_SUCCESS, foundChat: foundChat });
        })()
        : void null;
    });
    dispatch({type: SET_CURRENT_CHAT_UNSUB, unsub: unsub})
}

const toggleCurrentChatEdit = () => dispatch => {
    dispatch({type: TOGGLE_CURRENT_CHAT_EDIT});
}

const addMessageToChat = (chatId, message) => async dispatch => {
    dispatch({type: ADD_MESSAGE_TO_CHAT_PENDING});
    const chatRef = fire.firestore().collection('chats').doc(chatId);
    chatRef.update({
        lastMessage: message,
        modifiedAt: Date.now(),
    });
    chatRef.collection('messages').doc().set(message)
    .then(() => {
        dispatch({type: ADD_MESSAGE_TO_CHAT_SUCCESS});
    })
    .catch(error => {
        dispatch({type: ADD_MESSAGE_TO_CHAT_ERROR});
    })
}

const markMessageAsRead = (chatId, messageId, seenBy) => async dispatch => {
    dispatch({type: MARK_MESSAGE_AS_READ_PENDING});
    fire.firestore().collection('chats').doc(chatId).collection('messages').doc(messageId).update({
        seenBy: seenBy
    }).then(() => {
        dispatch({type: MARK_MESSAGE_AS_READ_SUCCESS});
    });
}

const chatReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_CHAT_PENDING: 
            return {
                ...state,
                create: {
                    ...state.create,
                    pending: true,
                }
            }
        case CREATE_CHAT_SUCCESS: 
            return {
                ...state,
                create: {
                    ...state.create,
                    pending: false,
                }
            }
        case CREATE_CHAT_ERROR: 
            return {
                ...state,
                create: {
                    ...state.create,
                    pending: false,
                }
            }
        case GET_CURRENT_CHAT_PENDING: 
            return {
                ...state,
                current: {
                    ...state.current,
                    pending: true,
                    id: action.chatId,
                    unsubs: [],
                    edit: {
                        visible: false,  
                        pending: false,
                    } 
                }
            }
        case GET_CURRENT_CHAT_SUCCESS: 
            return {
                ...state,
                current: {
                    ...state.current,
                    pending: false,
                    notFound: false,
                    imgPath: '',
                    imgUrl: '',
                    ...action.foundChat,
                    members: {
                        pure: [...action.foundChat.members],
                        ...state.current.members,
                    }
                }
            }
        case GET_CURRENT_CHAT_NOT_FOUND: 
            return {
                ...state,
                current: {
                    ...state.current,
                    pending: true,
                    notFound: true,
                    id: action.chatId,
                }
            }
        case TOGGLE_CURRENT_CHAT_EDIT: {
            return {
                ...state,
                current: {
                    ...state.current,
                    edit: {
                        ...state.current.edit,
                        visible: !state.current.edit.visible,
                    }
                }
            }
        }
        case SET_CURRENT_CHAT_UNSUB: 
            return { 
                ...state,
                current: {
                    ...state.current,
                    unsubs: [...state.current.unsubs, action.unsub],
                }
            }
        case GET_CURRENT_CHAT_MEMBERS_PENDING:  
            return {
                ...state,
                current: {
                    ...state.current,
                    members: {
                        ...state.current.members,
                        fetched: [],
                        pending: true,
                    }
                }
            }
        case GET_CURRENT_CHAT_MEMBERS_SUCCESS: 
            return { 
                ...state,
                current: {
                    ...state.current,
                    members: {
                        ...state.current.members,
                        fetched: [...action.fetchedMembers],
                        pending: false,
                    },
                }
            }
        case GET_CURRENT_CHAT_MESSAGES_PENDING: 
            return { 
                ...state,
                current: {
                    ...state.current,
                    messages: {
                        ...state.messages,
                        notFound: false,
                        pending: true,
                        list: [],
                    }
                }
            }
        case GET_CURRENT_CHAT_MESSAGES_SUCCESS: 
            return { 
                ...state,
                current: {
                    ...state.current,
                    messages: {
                        ...state.messages,
                        pending: false,
                        list: [...action.messages],
                    }
                }
            }
        case GET_CURRENT_CHAT_MESSAGES_NOT_FOUND: 
            return { 
                ...state,
                current: {
                    ...state.current,
                    messages: {
                        ...state.messages,
                        pending: false,
                        notFound: true,
                    }
                }
            }
        case GET_CHAT_LIST_SUCCESS: 
            return { 
                ...state,
                list: {
                    ...state.list,
                    pending: false,
                    items: action.chats,
                }
            }
        
        default: 
            return state;
    }
}

export {
    createNewChat,
    updateChat,
    getCurrentChat,
    toggleCurrentChatEdit,
    getChatList,
    getCurrentChatMembers,
    addMessageToChat,
    markMessageAsRead,
    chatReducer
}