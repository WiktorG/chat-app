import React from 'react'
import { connect } from 'react-redux'
import store from 'Services/store';

import { toggleModal } from 'Components/Modals/actions'
import { modals } from 'Components/Modals/selectors'
import { registerReducer, register } from './actions';
import { register as registerStore } from './selectors';
import { Spinner } from 'Components//LoginForm/styles';

import { Form, Button, Input, Icon, Row, Col, Modal, Checkbox, Spin } from 'antd'

const RegisterForm = (props) => {

    const handleSubmit = (e) => {
        e.preventDefault();
        props.form.validateFields((err, values) => {
            if (values.passwordRegister !== values.passwordRepeatRegister) {
                props.form.setFields({
                    passwordRegister: {
                        value: values.passwordRegister,
                        errors: [new Error('Passwords must be equal')],
                    },
                    passwordRepeatRegister: {
                        value: values.passwordRepeatRegister,
                        errors: [new Error('Passwords must be equal')],
                    }
                })
                return;
            }
            if (!values.privacyPolicy) {
                props.form.setFields({ 
                    privacyPolicy: {
                        value: values.privacyPolicy,
                        errors: [new Error('You must accept privacy policy')],
                    }
                })
                return;
            }
            if (!err) {
                const data = {
                    email: values.emailRegister,
                    password: values.passwordRegister,
                }
                props.register(data);
            }
        });
    }

    const { getFieldDecorator } = props.form;
    return (
        <Modal
            visible={props.modals.register}
            footer={false}
            title={"Register"}
            centered
            onCancel={() => props.toggleModal('register')}
        >
            <Form onSubmit={handleSubmit}>
                <Row gutter={4}>
                    <Col span={24}>   
                        <Form.Item>
                            {getFieldDecorator('emailRegister', {
                                rules: [
                                    { type: 'email', message: 'This must be an email!' },
                                    { required: true, message: 'Please input your email!' }
                                ],
                            })(
                                <Input
                                autoComplete="off"
                                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="e-mail"
                                />,
                            )}
                        </Form.Item>
                    </Col>
                    <Col md={12}>
                        <Form.Item>
                            {getFieldDecorator('passwordRegister', {
                                rules: [
                                    { required: true, message: 'Please input your password' },
                                    { min: 8, message: 'Password must by at least 8 characters long' }
                                ]
                            })(
                                <Input
                                autoComplete="off"
                                type="password"
                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="password"
                                />,
                            )}
                        </Form.Item>
                    </Col>
                    <Col md={12}>
                        <Form.Item>
                            {getFieldDecorator('passwordRepeatRegister', {
                                rules: [
                                    { required: true, message: 'Please input your password' },
                                    { min: 8, message: 'Password must by at least 8 characters long' }
                                ]
                            })(
                                <Input
                                autoComplete="off"
                                type="password"
                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="repeat password"
                                />,
                            )}
                        </Form.Item>
                    </Col>
                    <Col md={24}>
                        <Form.Item>
                        {getFieldDecorator('privacyPolicy', {
                            valuePropName: 'checked',
                            rules: [
                                { required: true, message: 'Accept privacy policy'}
                            ]
                        })(<Checkbox>I have read and I accept privacy policy and all the terms associated with this website.</Checkbox>)}
                        </Form.Item>
                    </Col>
                    <Col md={24}>
                        <Button type="primary" htmlType="submit" style={{ width: '100%' }}>
                            Register
                        </Button>
                    </Col>
                </Row>
                <Spinner style={{display: props.registerStore.spinner ? 'flex' : 'none'}}>
                    <Spin />
                </Spinner>
            </Form>
        </Modal>
    )
}

store.attachReducers({ register: registerReducer })

export default connect(state => ({
    modals: modals(state),
    registerStore: registerStore(state),
}), dispatch => ({
    toggleModal: modalName => dispatch(toggleModal(modalName)),
    register: params => dispatch(register(params))
}) )(Form.create()(RegisterForm))
