import fire from './../../services/firebase';

import { message } from 'antd';

export const REGISTER_PENDING = 'REGISTER_PENDING',
      REGISTER_SUCCESS = 'REGISTER_SUCCESS',
      REGISTER_FAIL = 'REGISTER_FAIL';

const initialState = {
    spinner: false,
}

const register = params => dispatch => {
    dispatch({type: REGISTER_PENDING});
    return fire.auth().createUserWithEmailAndPassword(params.email, params.password).then((data) => {
        dispatch({type: REGISTER_SUCCESS});
        message.success('Success!');
        return data;
    }).catch((err) => {
        dispatch({type: REGISTER_FAIL});
        message.error('Something went wrong - sorry :(');
        return err;
    })
}

const registerReducer = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_PENDING:
            return {
                ...state,
                spinner: true,
            }
        case REGISTER_SUCCESS:
            return {
                ...state,
                spinner: false,
            }
        case REGISTER_FAIL:
            return {
                ...state,
                spinner: false,
            }
        default:
            return state;
    }
}

export {
    registerReducer,
    register,
}