import fire from './../../services/firebase';

import { setUserId, getUserData } from './../User/actions';

export const APP_AUTHORIZE_PENDING = 'APP_AUTHORIZE_PENDING',
      APP_AUTHORIZE_SUCCESS = 'APP_AUTHORIZE_SUCCESS',
      APP_AUTHORIZE_FAIL = 'APP_AUTHORIZE_FAIL';

const initialState = {
    auth: {
        isAuthorized: false,
        fetching: false
    }
}

const appAuthListener = params => dispatch => {
    dispatch({type: APP_AUTHORIZE_PENDING});
    fire.auth().onAuthStateChanged((user) => {
        if (user) {
            dispatch(setUserId(user.uid));
            dispatch(getUserData(user.uid));
            dispatch({type: APP_AUTHORIZE_SUCCESS});
            localStorage.setItem('authorized', 'true');
        } else {
            dispatch({type: APP_AUTHORIZE_FAIL});
            localStorage.setItem('authorized', 'false');
        }
    })
}

const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case APP_AUTHORIZE_PENDING:
            return {
                ...state,
                auth: {
                    ...state.auth,
                    fetching: true,
                }
            }
        case APP_AUTHORIZE_SUCCESS:
            return {
                ...state,
                auth: {
                    ...state.auth,
                    isAuthorized: true,
                    fetching: false,
                }
            }
        case APP_AUTHORIZE_FAIL:
            return {
                ...state,
                auth: {
                    ...state.auth,
                    isAuthorized: false,
                    fetching: false,
                }
            }
        default:
            return state;
    }
}

export { 
    appReducer,
    appAuthListener,
}