import fire from './../../services/firebase';

export const LOGIN_PENDING = 'LOGIN_PENDING',
      LOGIN_SUCCESS = 'LOGIN_SUCCESS',
      LOGIN_FAIL = 'LOGIN_FAIL';

const initialState = {
    spinner: false,
}

const login = params => dispatch => {
    dispatch({type: LOGIN_PENDING});
    return fire.auth().signInWithEmailAndPassword(params.email, params.password).then((res) => {
        dispatch({type: LOGIN_SUCCESS});
    }).catch((error) => {
        dispatch({type: LOGIN_FAIL});
        throw new Error('Wrong email or password.');
    });

}

const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_PENDING:
            return {
                ...state,
                spinner: true,
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                spinner: false,
            }
        case LOGIN_FAIL:
            return {
                ...state,
                spinner: false,
            }
        default:
            return state;
    }
}

export {
    loginReducer,
    login,
}