import React from 'react'
import { connect } from 'react-redux'
import store from 'Services/store';

import { toggleModal } from 'Components/Modals/actions';
import { loginReducer, login } from './actions';
import { login as loginStore } from './selectors';

import { Icon, Input, Button, Spin } from 'antd';
import { Form, Spinner } from './styles';

const LoginForm = (props) => {   
    const handleSubmit = e => {
        e.preventDefault();
        props.form.validateFields((err, values) => {
          if (!err) {
            const data = {
                ...values
            }
            props.login(data).catch(error => {
                props.form.setFields({
                    email: {
                      value: data.email,
                      errors: [error], 
                    },
                    password: {
                     value: data.password,
                     errors: [error],
                    }
                });
            });                     
          }
        });
    }
    
    const { getFieldDecorator } = props.form;
    return (
        <Form onSubmit={handleSubmit}>
            <h2>Login</h2>
            <Form.Item>
                {getFieldDecorator('email', {
                    rules: [
                        { type: 'email', message: 'This must be an email!' },
                        { required: true, message: 'Please input your email!' }
                    ],
                })(
                    <Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder="e-mail"
                    />,
                )}
            </Form.Item>
            <Form.Item>
                {getFieldDecorator('password', {
                })(
                    <Input
                    type="password"
                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder="password"
                    />,
                )}
            </Form.Item>
            <Form.Item>
                <Button type="primary" htmlType="submit" style={{ width: '100%' }}>
                    Login
                </Button>
                Or <a href="/register" onClick={(e) => { e.preventDefault(); props.toggleModal('register') }}>register now!</a>
            </Form.Item>
            <Spinner style={{display: props.loginStore.spinner ? 'flex' : 'none'}}>
                <Spin />
            </Spinner>
        </Form>
    )
}

store.attachReducers({ login: loginReducer })

export default connect(state => ({
    loginStore: loginStore(state),
}), dispatch => ({
    toggleModal: modalName => dispatch(toggleModal(modalName)),
    login: params => dispatch(login(params)),
}) )(Form.create()(LoginForm))
