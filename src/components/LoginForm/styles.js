import styled from 'styled-components'
import { Form as AntForm } from 'antd'

const Form = styled(AntForm)`
    &.ant-form {
        position: relative;
        display: block;
        width: 100%;
        margin: 20px;
        padding: 20px 20px 0;
        max-width: 300px;
        background-color: #FFFFFF;
        border-radius: 4px;
        box-shadow: 0px 0px 30px rgba(0,0,0,0.1);
    }
`

const Spinner = styled.div` 
    position: absolute;
    left: 0;
    top: 0;
    display: flex;
    justify-content: center;
    align-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.8);
`

export { 
    Form,
    Spinner,
}