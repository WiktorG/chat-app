/* eslint-disable array-callback-return */
import fire from './../../services/firebase';

const searchUserByNickname = (nickname) => {
    return fire.firestore().collection('users').orderBy('nickname').startAt(nickname).endAt(nickname+'\uf8ff').limit(25).get().then(snapshot => {
        if ( !snapshot.empty ) {
            const foundUsers = snapshot.docs.map(doc => {
                if ( !doc.empty ) {
                    return {
                        id: doc.id, 
                        ...doc.data()
                    }
                }
            })
            return foundUsers;
        }
    });
}

export {
    searchUserByNickname
}