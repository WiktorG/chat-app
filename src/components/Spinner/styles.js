import styled from 'styled-components';

const SpinnerContainer = styled.div`
    width: 100%;
    height: 100vh;
    position: fixed;
    left: 0;
    top: 0;
    z-index: 1000;
    background-color: rgb(255, 255, 255);
    display: flex;
    justify-content: center;
    align-content: center;
    align-items: center;
`;

export {
    SpinnerContainer,
}