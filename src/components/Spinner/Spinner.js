import React from 'react';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import { SpinnerContainer } from './styles';

const Spinner = (props) => {
    return (
        <>{props.visible &&
        <SpinnerContainer>
            <Spin size={'large'}/>
        </SpinnerContainer>
        }
        </>
    )
}

Spinner.propTypes = {
    visible: PropTypes.bool.isRequired
}

export default Spinner
