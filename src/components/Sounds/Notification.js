import PropTypes from "prop-types";
import Notification from 'Sounds/notification.mp3';

const PlayNotification = () => {
    const notification = new Audio(Notification);
    notification.play();
}

PlayNotification.propTypes = {
    fileName: PropTypes.string.isRequired,
}

export default PlayNotification