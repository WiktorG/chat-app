
const initialState = {
    register: false,
    settings: false,
    userInfo: false,
    chatCreate: false,
    chatEdit: false,
}

const TOGGLE_MODAL = 'TOGGLE_MODAL';

const toggleModal = modalName => dispatch => {
    dispatch({type: TOGGLE_MODAL, modalName: modalName});
}

const modalsReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_MODAL:
            return {
                ...state,
                [action.modalName]: !state[action.modalName]
            }
        default:
            return state
    }
}

export { 
    modalsReducer,
    toggleModal,
}