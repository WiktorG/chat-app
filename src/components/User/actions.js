import fire from './../../services/firebase';
import { message } from 'antd';

import { APP_AUTHORIZE_FAIL } from './../App/actions';
import { toggleModal } from './../Modals/actions';
import { uploadFile } from './../Upload/actions'

export const SET_USER_ID = 'SET_USER_ID',
             GET_USER_DATA_PENDING = 'GET_USER_DATA_PENDING',
             GET_USER_DATA_SUCCESS = 'GET_USER_DATA_SUCCESS',
             GET_USER_DATA_FAIL = 'GET_USER_DATA_FAIL',
             UPDATE_USER_DATA_PENDING = 'UPDATE_USER_DATA_PENDING',
             UPDATE_USER_DATA_SUCCESS = 'UPDATE_USER_DATA_SUCCESS',
             UPDATE_USER_DATA_ERROR = 'UPDATE_USER_DATA_ERROR';

const initialState = {
    id: '',
    data: {
        fetching: true,
        notSet: false
    },
}

const setUserId = id => dispatch => {
    dispatch({type: SET_USER_ID, id: id});
}

const getUserData = id => dispatch => {
    dispatch({type: GET_USER_DATA_PENDING});
    fire.firestore().collection('users').doc(id).get().then(doc => {
        if (doc && doc.exists) {
            dispatch({type: GET_USER_DATA_SUCCESS, data: doc.data()});
        } else {
            dispatch({type: GET_USER_DATA_FAIL, data: undefined});
            dispatch(toggleModal('userInfo'));
        }
    }).catch(res => {
        dispatch({type: APP_AUTHORIZE_FAIL});
    })
}

const updateUserData = data => async dispatch => {
    dispatch({type: UPDATE_USER_DATA_PENDING});
    const img = data.img;
    let dataToPut = {
        description: data.description,
        nickname: data.nickname,
    }
    if (img) {
        const imgRes = await uploadFile(img, 'user_avatars');
        const imgUrl = await imgRes.url;    
        if (imgUrl.length !== 0) {
            dataToPut = {
                ...dataToPut,
                imgPath: imgRes.path,
                imgUrl: imgUrl,
            }
        }
    }
    fire.firestore().collection('users').doc(data.id).set(dataToPut, { merge: true }).then(res => {
        dispatch({type: UPDATE_USER_DATA_SUCCESS});
        dispatch(getUserData(data.id));
        dispatch(toggleModal('userInfo'));
        message.success('Success!');
    });
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_USER_DATA_PENDING:
            return {
                ...state,
                data: {
                    ...state.data,
                    fetching: true,
                }
            }
        case GET_USER_DATA_SUCCESS:
            return {
                ...state,
                data: { 
                    fetching: false,
                    ...action.data,
                },
            }
        case GET_USER_DATA_FAIL:
            return {
                ...state,
                data: { 
                    fetching: false,
                    notSet: true,
                }
            }
        case UPDATE_USER_DATA_PENDING: 
            return {
                ...state,
                data: { 
                    ...state.data,
                    fetching: true,
                }
            }
        case UPDATE_USER_DATA_SUCCESS: 
            return {
                ...state,
                data: { 
                    notSet: false,
                    fetching: false,
                }
            }
        case SET_USER_ID:
            return {
                ...state,
                id: action.id,
            }
        default:
            return state;
    }
}

export {
    userReducer,
    setUserId,
    getUserData,
    updateUserData,
}