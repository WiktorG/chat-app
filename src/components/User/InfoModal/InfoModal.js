import React, { useState } from 'react';
import { connect } from 'react-redux';

import { modals } from './../../Modals/selectors';

import { updateUserData } from './../actions';
import { toggleModal } from './../../Modals/actions';
import { getBase64 } from './../../Upload/actions';
import { beforeUpload } from './../../Upload/picture';

import { user } from './../selectors';

import { Form, Input, Button, Icon, Row, Col, Spin, Avatar } from 'antd';
import { Modal, Spinner, Upload } from './styles';



const UserInfoModal = (props) => {
    const [loadingImg, toggleLoading] = useState(false)
    const [userImg, setUserImg] = useState('');
    const [userImgFile, setUserImgFile] = useState(null);
    const AvatarUploadButton = () => (
        <div>
            {loadingImg ? <Icon type="loading" /> : <Avatar size="large" icon="user"/> }
            <div className="ant-upload-text">upload</div>
        </div>
    );

    const handleChange = info => {
        info.file.status === 'uploading' ? toggleLoading(true) : void null;
        info.file.status === 'done' || info.file.status === 'error' ? getBase64(info.file.originFileObj, imgUrl => {
            setUserImg(imgUrl);
            setUserImgFile(info.file.originFileObj);
            toggleLoading(false);
        }) : void null;
    }

    const handleSubmit = e => {
        e.preventDefault();
        props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
              const preparedData = {
                  id: props.user.id,
                  description: values.userDescription,
                  nickname: values.userNickname,
                  img: userImgFile,
              }
              props.updateUserData(preparedData);
          }
        });
    }
    const { getFieldDecorator } = props.form;
    return (
        <Modal 
            visible={props.modals.userInfo}
            footer={false}
            title={props.user.data.notSet ? 'Fill in basic info about you.' : 'Edit info about you.'}
            onCancel={() => !props.user.data.notSet ? props.toggleModal('userInfo') : void null}
            closable={!props.user.data.notSet}
            centered
        >
            <Form
                onSubmit={handleSubmit}
            >
                <Row gutter={8} >
                    <Col 
                        span={24}
                    >
                        <Upload 
                            listType="picture-card"
                            className="avatar-uploader"
                            showUploadList={false}
                            onChange={handleChange}
                            beforeUpload={beforeUpload}
                        >
                        {(() => {
                            if (userImg !== '') {
                                return (<img src={userImg} alt="avatar" className="upload-preview" />)
                            } else if (props.user.data.imgUrl && props.user.data.imgUrl !== '') {
                                return (<img src={props.user.data.imgUrl} alt="avatar" className="upload-preview" />)
                            } else {
                                return <AvatarUploadButton/>
                            }
                        })()}
                        </Upload>
                    </Col>
                    <Col span={24} style={{marginTop: '10px'}}>
                        <Form.Item>
                            {getFieldDecorator('userNickname', {
                                initialValue: props.user.data.nickname,
                                rules: [
                                    { required: true, message: 'Please input your nickname!' },
                                    { min: 2, message: 'Nickname must be at least 2 characters long!'}
                                ],
                            })(
                                <Input
                                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    placeholder="nickname"
                                />
                            )}
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                        <Form.Item>
                            {getFieldDecorator('userDescription', {
                                initialValue: props.user.data.description || '',
                            })(
                                <Input.TextArea
                                    placeholder="Short description"
                                >

                                </Input.TextArea>
                            )}
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                        <Button 
                            type="primary" 
                            style={{width: '100%'}}
                            htmlType="submit"
                        >
                            {props.user.data.notSet ? 'Add' : 'Edit'}
                        </Button>
                    </Col>
                </Row>
            </Form>
            <Spinner style={{display: props.user.data.fetching ? 'flex' : 'none'}}>
                <Spin />
            </Spinner>
        </Modal>
    )
}

export default connect(state => ({
    modals: modals(state),
    user: user(state),
}), dispatch => ({
    toggleModal: modalName => dispatch(toggleModal(modalName)),
    updateUserData: data => dispatch(updateUserData(data)),
}) )(Form.create()(UserInfoModal));
