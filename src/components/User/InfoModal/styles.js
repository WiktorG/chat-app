import styled from 'styled-components'
import { Modal as AntModal, Upload as AntUpload } from 'antd'

const Modal = styled(AntModal)`
    &.ant-modal {
       max-width: 340px;
    }
`

const Upload = styled(AntUpload)`
    & .ant-upload {
        margin-right: auto;
        margin-left: auto;
        float: none;
    }
`

const Spinner = styled.div` 
    position: absolute;
    left: 0;
    top: 0;
    display: flex;
    justify-content: center;
    align-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.8);
    z-index: 10;
`

export {
    Modal,
    Spinner,
    Upload,
}