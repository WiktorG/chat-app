import { applyMiddleware } from 'redux'
import { createStore } from 'redux-dynamic-reducer'
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

import loggerMiddleware from './loggers/reduxLogger';
import combinedReducers from './reducers';

const store = (() => {
    const middlewares = [loggerMiddleware, thunkMiddleware]
    const middlewareEnhancer = applyMiddleware(...middlewares);
    const composedEnhancers = composeWithDevTools(
        middlewareEnhancer
    );

    const store = createStore(combinedReducers, undefined, composedEnhancers);

    return store;
})()


export default store;
