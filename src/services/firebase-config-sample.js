import firebase from 'firebase';
import '@firebase/firestore';

/* 
IN ORDER FOR THIS APP YO WORK YOU NEED TO CHANGE THIS FILE'S NAME TO firebase.js 
AND FILL DATA BELOW WITH YOUR FIREBASE CONFIG
*/

const config = {
    apiKey: "",
    authDomain: "",
    databaseURL: "",
    projectId: "chatzakka",
    storageBucket: "",
    messagingSenderId: ""
};
  
const fire = firebase.initializeApp(config);

export default fire;
