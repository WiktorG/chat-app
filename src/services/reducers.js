import { combineReducers } from 'redux';
import { modalsReducer } from './../components/Modals/actions';
import { appReducer } from './../components/App/actions';
import { userReducer } from './../components/User/actions';


const combinedReducers = combineReducers({
    app: appReducer,
    modals: modalsReducer,
    user: userReducer,
})

export default combinedReducers;