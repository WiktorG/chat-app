import React from 'react'
import PublicLayout from './../../layout/PublicLayout/PublicLayout';

const PublicView = () => {
    return (
        <div>
            <PublicLayout />
        </div>
    )
}

export default PublicView
