import React from 'react';

import ChatLayout from '../../layout/ChatLayout/ChatLayout';

const PrivateView = () => {
    return (
        <div>
            <ChatLayout />
        </div>
    )
}

export default PrivateView
